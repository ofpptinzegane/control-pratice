<?php

namespace App\Http\Controllers;

use App\Models\Voiture;
use Illuminate\Http\Request;

class VoitureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $voitures = Voiture::paginate(20);
        return view("index",compact("voitures"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view("create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //! 
        //! 
        //! 
        //! 
        //! 
        $request->validate([
            "titre"=>"required|max:255",
            "pages"=>"required",
            "description"=>"required",
            "categorie_id"=>"required",
        ]);
        if($request->hasFile("image")){
            $imagePath = $request->file("image")->store("voiture/images","public");
            $request["image"] = $imagePath;
        }
        Voiture::create($request);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $voiture = Voiture::find($id);
        return view("edit",compact("voiture"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        Voiture::destroy($id);
    }
}
