@extends('layouts.app')
@section('title', 'Index Route')
@section('content')



    <button>Ajouter</button>

    <table border="1">
        <thead>
            <td>id</td>
            <td>icon</td>
            <td>titre</td>
            <td>pages</td>
            <td>description</td>
            <td>categorie_id</td>
            <td>action</td>
        </thead>

        <tbody>
            @foreach ($voitures as $voiture)
                <td>{{ $voiture->id }}</td>
                <td>{{ $voiture->image }}</td>
                <td>{{ $voiture->titre }}</td>
                <td>{{ $voiture->pages }}</td>
                <td>{{ $voiture->description }}</td>
                <td>{{ $voiture->categorie_id }}</td>
                <td><a href="/voiture/edit">Edit</a><a href="/voiture/supprimer">Supprimer</a></td>
                @endforeach
        </tbody>

    </table>
