<?php

use App\Http\Middleware\AuthMidlleware;
use App\Models\Voiture;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VoitureController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::resource("voiture", VoitureController::class)->middleware(AuthMidlleware::class);
